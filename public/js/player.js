var isPlaying = false;
var player = document.getElementById('pm-player');
var currentAlbum = false;

function play(album) {
  var progress = document.getElementById('pm-player-progress-bar-' + album);

  if (isPlaying) {
    if (currentAlbum === album) {
      player.pause();
      changePlayButtonIcon(album, 'play');
    } else {
      playMusic(album, lastPlayed[album]);
    }
  } else {
    playMusic(album, lastPlayed[album]);
  }
  player.onloadedmetadata = function() {
    progress.max = player.duration;
  };

  player.addEventListener("timeupdate", function() {
    progress.value = player.currentTime;
  });

  player.addEventListener("ended", function(){
    next(album);
  });
}

player.onplaying = function() {
  isPlaying = true;
};

player.onpause = function() {
  isPlaying = false;
};

function next(album) {
  newTrack = (lastPlayed[album] + 1);
  if (newTrack >= songs[album].length) {
    newTrack = 0;
  }
  playMusic(album, newTrack);
}

function prev(album) {
  newTrack = (lastPlayed[album] - 1);
  if (newTrack <= 0) {
    newTrack = (songs[album].length - 1);
  }
  playMusic(album, newTrack);
}

function playMusic(album, track) {
  var displayName = document.getElementById('pm-player-track-name-' + album);

  displayName.innerHTML = trackNames[album][track];

  changePlayButtonIcon(album, 'pause');
  markPlayingTitleInTitleList(album, track);
  player.src = songs[album][track];
  player.load();
  player.play();
  lastPlayed[album] = track;
  currentAlbum = album;
}

function changePlayButtonIcon(album, whichIcon) {
  var pauseImage = "images/icons/pause.svg.php?fill-color=" + buttonColor;;
  var playImage = "images/icons/play.svg.php?fill-color=" + buttonColor;;
  for (var i = 0; i < songs.length; i++) {
    var buttonId = 'pm-play-button-icon-' + i;
    if (album == i) {
      if (whichIcon === 'play') {
        document.getElementById(buttonId).src = playImage;
      } else {
        document.getElementById(buttonId).src = pauseImage;
      }
    } else {
      document.getElementById(buttonId).src = playImage;
    }
  }
}

function markPlayingTitleInTitleList(album, title) {
  for (var i = 0; i < songs[album].length; i++) {
    if (i == title) {
      document.getElementById('pm-track-' + album + '-' + i).classList.add('pm-isplaying');
    } else {
      document.getElementById('pm-track-' + album + '-' + i).classList.remove('pm-isplaying');
    }
  }
}
