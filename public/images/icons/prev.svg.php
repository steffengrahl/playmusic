<?php header('Content-type: image/svg+xml');
echo '<?xml version="1.0" standalone="no"?>' ?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100px" height="100px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
	 <path d="M0 0 L0 100 L15 100 L15 65 L55 100 L55 70 L95 100 L95 0 L55 30 L55 0 L15 35 L15 0 Z"  style="fill: <?php echo '#'.$_GET['fill-color']; ?>;"/>
</svg>
