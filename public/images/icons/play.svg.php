<?php header('Content-type: image/svg+xml');
echo '<?xml version="1.0" standalone="no"?>' ?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="70px" height="100px" viewBox="0 0 70 100" enable-background="new 0 0 70 100" xml:space="preserve">
	 <polygon points="10,0 10,100 70,50" style="fill: <?php echo '#'.$_GET['fill-color']; ?>;"/>
</svg>
