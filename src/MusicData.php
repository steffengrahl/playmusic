<?php

namespace App;

class MusicData
{
    private $title_list_limit = 30;
    private $album = [];
    private $titles = [];
    private $artists = [];
    private $selectedAlbum = '';

    public function __construct()
    {
        $this->loadData();
    }

    /**
     *  reads the my_music.csv file and splits every line at ";". Than assigns the data
     *  from column 0, 1 and 3 to the titles member.
     *
     */
    private function loadData()
    {
        $data    = file('../data/my_music.csv');
        $titles  = [];
        $album   = [];
        $albumID = [];
        $artist  = [];

        if (!empty($data)) {
            for ($i = 1, $iMax = \count($data); $i < $iMax; $i++) {
                $dataFields = explode(';', $data[$i]);

                foreach ($dataFields as $key => $value) {
                    $dataFields[$key] = str_replace(["\r\n", "\n\r", "\n", "\r", '"'], '', $value);
                }

                if (empty($dataFields[2])) {
                    $dataFields[2] = 'Unbenanntes Album';
                }

                if (!\in_array($dataFields[2], $albumID, true)) {
                    $albumID[] = $dataFields[2];
                    $album[] = [$dataFields[2], $this->loadCover($dataFields[2])];
                }


                if (empty($dataFields[4])) {
                    $dataFields[4] = 'Unbekannter Artist';
                }

                if (!\in_array($dataFields[4], $artist, true)) {
                    $artist[] = $dataFields[4];
                }

                $artistID = array_flip($artist);
                $albumID = array_flip($albumID);
                $titles[] = [
                    $dataFields[1],
                    @$albumID[$dataFields[2]],
                    $dataFields[3],
                    @$artistID[$dataFields[4]]
                ];
                $albumID = array_flip($albumID);
            }
        }

        $this->artists = $artist;
        $this->album = $album;
        $this->titles = $titles;
    }

    /**
     *  looks if the cover image is in the assigned directory and returns filename +
     *  extension.
     *
     * @param string $albumName
     *
     * @return bool|string
     */
    private function loadCover($albumName)
    {
        $ext = ['.jpeg', '.jpg', '.gif', '.png', '.webp'];
        $albumName = urlencode($albumName);

        foreach ($ext as $value) {
            if (is_readable('PlayMusic/cover_images/' . $albumName . $value)) {
                return $albumName . $value;
            }
        }

        return false;
    }

    /**
     * returns the title list limit.
     *
     * @return int
     */
    public function getTitleListLimit()
    {
        return $this->title_list_limit;
    }

    /**
     * set the title list limit to a given value.
     *
     * @param int $limit
     */
    public function setTitleListLimit($limit)
    {
        if (!empty($limit) && \is_int($limit)) {
            $this->title_list_limit = $limit;
        }
    }

    /**
     *  sets the selected album value.
     *
     *
     * @param string $albumName
     *
     * @return bool
     */
    public function setSelectedAlbum($albumName)
    {
        if ($this->selectedAlbum = $albumName) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getAlbum()
    {
        $ret = [];
        foreach ($this->album as $i => $iValue) {

            if (!empty($this->selectedAlbum)) {
                if ($this->selectedAlbum === $this->album[$i][0]) {
                    $ret[] = [
                        'id' => $i,
                        'name' => $this->album[$i][0],
                        'cover' => $this->album[$i][1],
                        'titles' => $this->getTitles(0, $i)
                    ];
                }
            } else {
                $ret[] = [
                    'id' => $i,
                    'name' => $this->album[$i][0],
                    'cover' => $this->album[$i][1],
                    'titles' => $this->getTitles(0, $i)
                ];
            }
        }

        return $ret;
    }


    /**
     * @param int $start
     * @param int|null $albumID
     *
     * @return array|bool
     */
    public function getTitles($start, $albumID = null)
    {
        $ret = [];
        $step = $this->title_list_limit;

        if ($start > \count($this->titles)) {
            return false;
        }

        if (($start + $step) > \count($this->titles)) {
            $step = \count($this->titles);
        } else {
            $step += $start;
        }

        for ($i = $start; $i < $step; $i++) {
            if ($albumID !== null && $albumID === $this->titles[$i][1]) {
                $ret[] = [
                    'name' => $this->titles[$i][0],
                    'uri' => $this->titles[$i][2]
                ];
            }

            if ($albumID === null) {
                $ret[] = [
                    'name' => $this->titles[$i][0],
                    'uri' => $this->titles[$i][2]
                ];
            }
        }

        return $ret;
    }
}
