<?php

namespace App;

use App\MusicData;
use App\PlayerSkin;

class MusicPlayer
{
    
    /** name of the player skin to be used. */
    private $player_skin = 'titleList';
    /** output starts with this key. */
    private $firstTitle = 0;
    /** data object. */
    private $model;
    /** view object. */
    private $view;
    
    /** constructor.
     *
     * @param null $settings
     */
    public function __construct($settings)
    {
        $this->model = new MusicData();
        $this->view = new PlayerSkin();
        
        if (!empty($settings)) {
            $this->configuratePlayer($settings);
        }
    }
    
    /**
     *  sets the player setting to the values given in the settings array.
     *
     * @param array $settings
     */
    private function configuratePlayer(array $settings) :void
    {
        
        if (\is_array($settings)) {
            if (!empty($settings['player_skin'])) {
                $this->player_skin = $settings['player_skin'];
            }
            if (!empty($settings['title_per_page'])) {
                $this->model->setTitleListLimit($settings['title_per_page']);
            }
            if (!empty($settings['button_color'])) {
                $this->view->setButtonColor($settings['button_color']);
            }
            if (!empty($settings['download_titles'])) {
                $this->view->setShowDownloadLinks($settings['download_titles']);
            }
            if (!empty($settings['show_covers'])) {
                $this->view->setShowCoverImages($settings['show_covers']);
            }
            if (!empty($settings['show_album_names'])) {
                $this->view->setShowAlbumNames($settings['show_album_names']);
            }
        }
        
    }
    
    /**
     *  sets the given album name as value of $model-selectedAlbum.
     *
     * @param  (String) $albumName - album name to be set as selected album
     */
    public function selectAlbum($albumName)
    {
        if (!empty($albumName) && \is_string($albumName)) {
            $albumName = strip_tags($albumName);
            $this->model->setSelectedAlbum($albumName);
        }
    }
    
    /**
     *  returns the HTML representation of the music player.
     */
    public function init() :void
    {
        
        switch ($this->player_skin) {
            case 'album':
                echo $this->view->albumView($this->model->getAlbum());
                break;
            
            case 'titleList':
                echo $this->view->titleList($this->model->getTitles($this->firstTitle));
                break;
            
            default:
                echo $this->view->titleList($this->model->getTitles($this->firstTitle));
                break;
        }
    }
}
