<?php

namespace App;

class PlayerSkin
{

    /** should album names be shown. */
    private $show_album_names = false;
    /** should download links be shown. */
    private $show_download_links = false;
    /** should cover images be shown. */
    private $show_cover_images = false;
    /** color for play/pause/skip/download/lyrics button. */
    private $button_color = '000';

    public function __construct()
    {

    }

    /**
     *  sets the button color to a given value. must be a hexadecimal value with a length of 3 or 6.
     *
     * @param  (String) $color - new button color
     */
    public function setButtonColor($color)
    {
        if (preg_match('/[0-9a-fA-F]/', $color) && (\strlen($color) === 3 xor \strlen($color) === 6)) {
            $this->button_color = $color;
        }
    }

    /**
     *  sets the show album names value.
     *
     * @param (mixed) $value - boolean value or the integer counterpart to be set
     */
    public function setShowAlbumNames($value)
    {
        if (0 === $value) {
            $this->show_album_names = false;
        }

        if (1 === $value) {
            $this->show_album_names = true;
        }

        if (false === $value) {
            $this->show_album_names = false;
        }

        if (true === $value) {
            $this->show_album_names = true;
        }
    }

    /**
     *  sets the show download links value.
     *
     * @param (mixed) $value - boolean value or the integer counterpart to be set
     */
    public function setShowDownloadLinks($value)
    {
        if (0 === $value) {
            $this->show_download_links = false;
        }

        if (1 === $value) {
            $this->show_download_links = true;
        }

        if (false === $value) {
            $this->show_download_links = false;
        }

        if (true === $value) {
            $this->show_download_links = true;
        }
    }

    /**
     *  sets the show cover images value.
     *
     * @param (mixed) $value - boolean value or the integer counterpart to be set
     */
    public function setShowCoverImages($value)
    {
        if (0 === $value) {
            $this->show_cover_images = false;
        }

        if (1 === $value) {
            $this->show_cover_images = true;
        }

        if (false === $value) {
            $this->show_cover_images = false;
        }

        if (true === $value) {
            $this->show_cover_images = true;
        }
    }

    /**
     *  returns the view of one or more album with album art and title list.
     *
     * @param array $album
     *
     * @return string
     */
    public function albumView(array $album)
    {
        /* string to be returned */
        $ret = '';
        /* name of the album */
        $albumName = '';
        /* cover image of the album. */
        $albumCover = '';
        /* songs which build the JavaScript playlist */
        $uris = [];
        /* track names to display in player */
        $names = [];
        /* information for lastPlayed and lastPosition */
        $last = [];

        foreach ($album as $i => $iValue) {
            list($uris[], $names[]) = $this->trackNamesAndURIs($album[$i]['titles']);
            $last[] = 0;

            if (true === $this->show_album_names) {
                $albumName = '<h2 class="pm-album-name">' . $album[$i]['name'] . '</h2>';
            }

            if (true === $this->show_cover_images && !empty($album[$i]['cover'])) {
                $albumCover = '
          <div class="pm-cover">
            <img src="images/cover_images/' . $album[$i]['cover'] . '" class="pm-cover-image">
          </div>';
            }

            $ret .= '
        <div class="pm-album">
          ' . $albumName . '' . $albumCover . $this->controls($i) . '      <div class="pm-titles">' . $this->titleList($album[$i]['titles'],
                    $i) . '</div>
        </div>
        ';
        }

        $ret .= $this->player(implode(', ', $uris), implode(', ', $names),
            implode(', ', $last), implode(', ', $last));

        return $ret;
    }

    /**
     *  returns track uris and names as strings to be the JavaScript arrays later.
     *
     * @param array $titles - array with all the titles
     *
     * @return array - prepared strings for the JavaScript arrays
     */
    private function trackNamesAndURIs(array $titles)
    {
        $uris = [];
        $names = [];

        foreach ($titles as $i => $iValue) {
            if (!empty($titles[$i]['uri'])) {
                $uris[] = '"music/' . $titles[$i]['uri'] . '"';
                $names[] = '"' . $titles[$i]['name'] . '"';
            }
        }

        return ['[' . implode(', ', $uris) . ']', '[' . implode(', ', $names) . ']'];
    }

    /**
     *  returns the HTML5 code to play an audio file in the browser.
     *
     * @param int $controls_id
     *
     * @return string
     */
    private function controls(int $controls_id)
    {
        return '
          <div class="pm-player-frame"><p class="pm-player-track-name" id="pm-player-track-name-' . $controls_id . '"></p><button type="button" class="pm-play-pause-button" id="pm-play-button-' . $controls_id . '" onclick="play(' . $controls_id . ')"><img src="images/icons/play.svg.php?fill-color=' . urlencode($this->button_color) . '" class="pm-play-pause-button-icon" id="pm-play-button-icon-' . $controls_id . '"></button><button type="button" id="pm-prev-button-' . $controls_id . '" class="pm-button" onclick="prev(' . $controls_id . ')"><img src="images/icons/prev.svg.php?fill-color=' . urlencode($this->button_color) . '"></button><button type="button" id="pm-next-button-' . $controls_id . '" class="pm-button" onclick="next(' . $controls_id . ')"><img src="images/icons/next.svg.php?fill-color=' . urlencode($this->button_color) . '"></button><progress id="pm-player-progress-bar-' . $controls_id . '" class="pm-player-progress-bar" max="100" value="0"></progress></div>
    ';
    }

    /**
     *  returns the view for the title list.
     *
     * @param array $titles
     * @param int $albumID
     *
     * @return string
     */
    public function titleList(array $titles, $albumID = 0)
    {
        $trackID = 0;
        $ret = '<ol class="pm-title-list">';
        foreach ($titles as $i => $iValue) {
            $ret .= '<li class="pm-title-list-item"><div class="player-single';
            if (!empty($titles[$i]['uri'])) {
                $ret .= ' pm-isplayable" id="pm-track-' . $albumID . '-' . $trackID . '" onclick="playMusic(' . $albumID . ', ' . $trackID . ')"';
                $trackID++;
            } else {
                $ret .= '"';
            }
            $ret .= '><span class="title-name">' . $titles[$i]['name'] . '</span></div>';
            $ret .= '</li>';
        }
        $ret .= '</ol>';

        return $ret;
    }

    /**
     *  returns the player at the end of the list and all the JavaScript arrays.
     *
     * @param string $songs
     * @param string $track_names
     * @param string $last_played
     * @param string $last_position
     *
     * @return string
     */
    private function player($songs, $track_names, $last_played, $last_position)
    {
        return '
        <audio id="pm-player" src=""></audio>

        <script type="text/javascript">
        <!--
          var songs = [' . $songs . '];
          var trackNames = [' . $track_names . '];
          var lastPlayed = [' . $last_played . '];
          var lastPosition = [' . $last_position . '];
          var buttonColor = "' . $this->button_color . '";
        -->
        </script>
        <script src="js/player.js" type="text/javascript"></script>
    ';
    }
}
